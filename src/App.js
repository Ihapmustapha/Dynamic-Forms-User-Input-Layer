import React, { Component } from 'react';
import './App.css';
import PersonalData from './components/personalData/PersonalData.js';


class App extends Component {
  render() {
    return (
      <div className="App">
        <PersonalData />
      </div>
    );
  }
}

export default App;
