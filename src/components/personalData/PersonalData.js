import React, { Component } from 'react';
import './PersonalData.css'; 
import Downshift from 'downshift'; 


class PersonalData extends Component {
    constructor() {
        super(); 
        this.state = {
            schema : {
                title: "Event Form",
                type: "object",
                properties: {
                  basicInfo: {
                    title: "Baisc Information",
                    type: "object",
                    required: ["firstName", "lastName", "phoneNum", "email"],
                    properties: {}
                  },
                  
                  extraInfo: {
                    title: "Extra Information",
                    type: "object",
                    properties: {
                      hobbies: {
                        title: "your Hobby",
                        type: "string",
                        enum: ["football", "babyfoot", "swimming", "what"]
                      },
                      q1: {
                        title: "What is your favourite movie ?",
                        type: "string",
                      }
                    }
                  }
                }
            },

            questions: [
                {value: "How old are you?"},
                {value: "How is your momma?"},
                {value: "What is your favorite singer?"},
                {value: "Why did you come here?"},
                {value: "What are your interests?"},
                {value: "What is your age?"},
                {value: "What is your nationality?"},
                {value: "What is your job title?"},
                {value: "Hi"},
                {value: "Hi there"},
            ], 

            selectedQuestions: [], 
            
            
        }

        //binding event handlers and other methods
        this.getCheckedboxesFor = this.getCheckedboxesFor.bind(this); 
        this.createForm = this.createForm.bind(this); 
    }
    render() {
        return (
            <div className='container'>
                <div className='col-md-12'>
                    <div className='card'>
                        <label>Baisc Information</label>
                        {/* Multiple Selection Compoenent */}
                        
                        <div className="check-box">
                            <label>
                            <input type="checkbox"
                            className="basicInfo"
                            id="firstName"
                            value="firstName"
                            ></input>
                            First Name</label>
                        </div>
                        <div className="check-box">
                            <label>
                            <input type="checkbox"
                                className="basicInfo"
                                id="lastName"
                                value="lastName"></input>
                            Last Name</label>
                        </div>
                        <div className="check-box">
                            <label>
                            <input type="checkbox"
                                className="basicInfo"
                                id="phoneNumber"
                                value="phoneNumber"></input> 
                            Phone Number</label>
                        </div>
                        <div className="check-box">
                            <label>
                            <input type="checkbox"
                                className="basicInfo"
                                id="eMail"
                                value="eMail"></input> 
                            E-mail</label>
                        </div>
                        <div className="check-box">
                            <label>
                            <input type="checkbox"
                                className="basicInfo"
                                id="birthDate"
                                value="birthDate"></input> 
                            Birth Date</label>
                        </div>
                        <div className="check-box">
                            <label>
                            <input type="checkbox"
                                className="basicInfo" 
                                id="gender"
                                value="gender"></input> 
                            Gender</label>
                        </div>
                        {/* An implemented API by PayPal with only two dependencies to handle auto-complete  */}
                        <Downshift
                            onChange={selection => this.addSelection(selection.value)}
                            itemToString={item => (item ? item.value: '')}
                        >
                        {({ getInputProps, 
                            getItemProps, 
                            getLabelProps,
                            isOpen, 
                            inputValue, 
                            highlightedIndex, 
                            selectedItem,
                        })=> (
                            <div>
                                <label {...getLabelProps()}>Type Your Question</label>
                                <input {...getInputProps({placeholder: 'Type Here'})}/> 
                                {isOpen? (
                                    <div>
                                        {this.state.questions
                                            .filter(item => !inputValue || item.value.toLowerCase().includes(inputValue.toLocaleLowerCase()))
                                            .map((item, index) => (
                                                <div
                                                    {...getItemProps({
                                                        key: item.value,
                                                        index,
                                                        item,
                                                        style: {
                                                            backgroundColor: 
                                                                highlightedIndex === index ? 'lightgray' : 'white',
                                                                fontweight: selectedItem === item ? 'bold' : 'normal',
                                                        },
                                                    })}
                                                >
                                                    {item.value}
                                                </div>
                                            ))}
                                    </div>
                                ) : null}
                            </div>
                        )}
                        </Downshift>
                        {/* Render Selected Questions */}
                        <ul>
                            {this.state.selectedQuestions.map((i, index) => 
                                <span key={i}>
                                <li>{i}</li>
                                <button className='btn btn-danger' onClick={()=>this.deleteSelection(index)}>Delete</button>
                                </span>
                            )}
                        </ul>
                        {/* Create Form Button Component */}
                        <button className='btn btn-primary' onClick={this.createForm}>Create Form</button>
                    </div> 
                </div>
            </div> 
        ); 
    }

    createForm(e) {
        let values = this.getCheckedboxesFor(".basicInfo");
        let schema = Object.assign({}, this.state.schema);
        let properties = []
        for (let i=0; i<values.length; i++) {
            switch (values[i]) {
                case 'firstName':
                    properties["firstName"] = {title: "First Name", type: "string"}; 
                break;

                case 'lastName': 
                    properties["lastName"] = {title: "Last Name", type: "string"}; 
                break; 

                case 'phoneNumber': 
                    properties["phoneNum"] = {title: "Phone Number", type: "number", "minLength": 10};
                break; 

                case 'eMail':
                    properties["email"] = {title: "Email", type: "string", format: "email"};
                break; 
                case 'birthDate':
                    properties["birthDate"] = {title: "Birth Date", type: "string", format: "date"};
                break; 

                case 'gender':
                    properties["gender"] = {title: "Gender", type:"boolean", enumNames: ["male", "female"]}; 
                break; 
                default: 
                console.log("No Data Changed"); 
            }
        }
        schema.properties.basicInfo.properties = properties;
        this.setState({schema});
        console.log(schema); 
    }

    //The Method takes a Selector as an argument
    getCheckedboxesFor(selector) { 
        //Getting All Items with the passed 'Selector' 
        let checked = document.querySelectorAll(selector + "[type='checkbox']:checked");  
        let values = []; 
        //Converting the NodeList returned from querySelectorAll to an Array 
        for (let i=0; i<checked.length; i++) {
            values[i] = checked[i].value; 
        }
        return values;
    }

    //Adding Question to the Array
    addSelection = (selection) => {
        let selected = this.state.selectedQuestions;
        selected.push(selection);
        this.setState({selectedQuestions: selected}); 
        console.log(this.state.selectedQuestions); 
    }
    //Deleting Question from the Array 
    deleteSelection = (index) => {
        let selected = this.state.selectedQuestions; 
        selected.splice(index, 1); 
        this.setState({selectedQuestions: selected}); 
        console.log(this.state.selectedQuestions); 
    }
    
}

export default PersonalData; 