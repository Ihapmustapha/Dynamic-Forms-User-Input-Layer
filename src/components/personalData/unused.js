// import React, { Component } from 'react';
// import './PersonalData.css'; 

// class PersonalData extends Component {
//     constructor() {
//         super(); 
//         this.state = {
//             schema : {
//                 title: "Event Form",
//                 type: "object",
//                 properties: {
//                   basicInfo: {
//                     title: "Baisc Information",
//                     type: "object",
//                     required: ["firstName", "lastName", "phoneNum", "email"],
//                     properties: {}
//                   },
//                   //Ends Here// 
//                   //Same Logic will be applied to this one// 
//                   extraInfo: {
//                     title: "Extra Information",
//                     type: "object",
//                     properties: {
//                       hobbies: {
//                         title: "your Hobby",
//                         type: "string",
//                         enum: ["football", "babyfoot", "swimming", "what"]
//                       },
//                       q1: {
//                         title: "What is your favourite movie ?",
//                         type: "string",
              
//                       }
//                     }
//                   }
//                 }
//               }
            
//         }

//         //binding event handlers 
//         this.handleBasicInfoSelect = this.handleBasicInfoSelect.bind(this);
//         this.getCheckedboxesFor = this.getCheckedboxesFor.bind(this); 
//         this.createForm = this.createForm.bind(this); 
//         this.getCheckedEventListener = this.getCheckedEventListener.bind(this); 
//     }
//     render() {
//         return (
//             <div className='container'>
//                 <div className='col-md-12'>
//                     <div className='card'>
//                         <label>Baisc Information</label>
//                         {/* Multiple Selection Compoenent */}
                        
//                         <div className="check-box">
//                             <label>
//                             <input type="checkbox"
//                             name="basicInfo"
//                             id="firstName"
//                             value="firstName"
//                             onChange={this.handleBasicInfoSelect}></input>
//                             First Name</label>
//                         </div>
//                         <div className="check-box">
//                             <label>
//                             <input type="checkbox"
//                                 name="basicInfo"
//                                 id="lastName"
//                                 value="lastName"
//                                 onChange={this.handleBasicInfoSelect}></input>
//                             Last Name</label>
//                         </div>
//                         <div className="check-box">
//                             <label>
//                             <input type="checkbox"
//                                 name="basicInfo"
//                                 id="phoneNumber"
//                                 value="phoneNumber"
//                                 onChange={this.handleBasicInfoSelect}></input> 
//                             Phone Number</label>
//                         </div>
//                         <div className="check-box">
//                             <label>
//                             <input type="checkbox"
//                                 name="basicInfo"
//                                 id="eMail"
//                                 value="eMail"
//                                 onChange={this.handleBasicInfoSelect}></input> 
//                             E-mail</label>
//                         </div>
//                         <div className="check-box">
//                             <label>
//                             <input type="checkbox"
//                                 name="basicInfo"
//                                 id="birthDate"
//                                 value="birthDate"
//                                 onChange={this.handleBasicInfoSelect}></input> 
//                             Birth Date</label>
//                         </div>
//                         <div className="check-box">
//                             <label>
//                             <input type="checkbox"
//                                 name="basicInfo" 
//                                 id="gender"
//                                 value="gender"
//                                 onChange={this.handleBasicInfoSelect}></input> 
//                             Gender</label>
//                         </div>
                        
//                         {/* Filter Button Component */}
//                         <span>
//                         <button className='btn btn-primary' onClick={this.createForm}>Create Form</button>
//                         <button className='btn btn-primary' onClick={this.getCheckedEventListener}>Get checkbox values</button> 
//                         </span>
//                     </div> 
//                 </div>
//             </div> 
//         ); 
//     }

//     //Handling multiselection from multiple check boxs  
//     handleBasicInfoSelect(e) {
//         let firstName = document.getElementById("firstName").checked;
//         let lastName = document.getElementById("lastName").checked;
//         let phoneNumber = document.getElementById("phoneNumber").checked;
//         let eMail = document.getElementById("eMail").checked; 
//         let birthDate = document.getElementById("birthDate").checked; 
//         let gender = document.getElementById("gender").checked; 
//         let schema = Object.assign({}, this.state.schema);
        
//         if (firstName) {
//             schema.properties.basicInfo.properties["firstName"] = {title: "First Name", type: "string"}; 
//         } else if (!firstName) {
//             delete schema.properties.basicInfo.properties["firstName"]; 
//         }

//         if (lastName) {
//             schema.properties.basicInfo.properties["lastName"] = {title: "Last Name", type: "string"}; 
//         } else if (!firstName) {
//             delete schema.properties.basicInfo.properties["lastName"];
//         }

//         if (phoneNumber) {
//             schema.properties.basicInfo.properties["phoneNum"] = {title: "Phone Number", type: "number", "minLength": 10};
//         } else if (!phoneNumber) {
//             delete schema.properties.basicInfo.properties["phoneNum"];
//         }

//         if (eMail) {
//             schema.properties.basicInfo.properties["email"] = {title: "Email", type: "string", format: "email"};
//         } else if (!eMail) {
//             delete schema.properties.basicInfo.properties["email"];
//         }

//         if (birthDate) {
//             schema.properties.basicInfo.properties["birthDate"] = {title: "Birth Date", type: "string", format: "date"};
//         } else if (!birthDate) {
//             delete schema.properties.basicInfo.properties["birthDate"];
//         }

//         if (gender) {
//             schema.properties.basicInfo.properties["gender"] = {title: "Gender", type:"boolean", enumNames: ["male", "female"]}; 
//         } else if (!gender) {
//             delete schema.properties.basicInfo.properties["gender"];
//         }
        
//         this.setState({schema}); 
//     }
//     //Logging the 'Filter Object' wich containes the parameters to be sent.
//     //Should be modified next time to send the Query to the back-end. 
//     getCheckedEventListener (e) {
//         this.getCheckedboxesFor("basicInfo");
//     }
    
//     getCheckedboxesFor (checkboxName) {
//         const checkboxes = document.querySelectorAll(checkboxName), values =[]; 
//         Array.prototype.forEach.call(checkboxes, function(el){
//             values.push(el.value);
//         });
//         console.log(values); 
//     }
//     createForm(e) {
//         console.log(this.state.schema);
//     }


// }

// export default PersonalData; 
